package pu.fmi.wordle.logic;

import static java.lang.String.format;

public class MaxAttemptsReachedException extends RuntimeException{

    public MaxAttemptsReachedException() {
        super("Max attempts reached!");
    }
}
